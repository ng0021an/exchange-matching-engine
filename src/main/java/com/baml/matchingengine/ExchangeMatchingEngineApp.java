package com.baml.matchingengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by QuanNguyenHuu on 29/6/18.
 */
@SpringBootApplication
public class ExchangeMatchingEngineApp {

    public static void main(String[] args) {
        SpringApplication.run(ExchangeMatchingEngineApp.class, args);
    }

}
