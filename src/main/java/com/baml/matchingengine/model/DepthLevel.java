package com.baml.matchingengine.model;

/**
 * Created by QuanNguyenHuu on 1/7/18.
 */
public interface DepthLevel {
    boolean isMatchable(RestingOrder newRequest);
    boolean fulfillNewRequest(RestingOrder newRequest);
    boolean isEmpty();
}
