package com.baml.matchingengine.model;

import com.baml.matchingengine.data.*;
import com.baml.matchingengine.publish.ExecutionListener;

import java.time.ZonedDateTime;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 * Used in internal data structure for matching
 * Main differences are quantity is mutable (to optimize changing liquidity for top of book)
 * and execution publication
 * Also implements DepthLevel interface to act as a simple depth level with single order
 * in SimpleMatchingEngine
 */
public class RestingOrder implements DepthLevel {

    private final NewOrderRequest newOrderRequest;
    private final ExecutionListener executionListener;

    private long quantity;

    public RestingOrder(NewOrderRequest newOrderRequest, ExecutionListener executionListener) {
        this.newOrderRequest = newOrderRequest;
        this.executionListener = executionListener;
        this.quantity = newOrderRequest.getQuantity();
    }

    public NewOrderRequest getNewOrderRequest() {
        return newOrderRequest;
    }

    public long getId() {
        return newOrderRequest.getId();
    }

    public String getSymbol() {
        return newOrderRequest.getSymbol();
    }

    public Side getSide() {
        return newOrderRequest.getSide();
    }

    public double getPrice() {
        return newOrderRequest.getPrice();
    }

    public ZonedDateTime getTime() {
        return newOrderRequest.getTime();
    }

    public long getQuantity() {
        return quantity;
    }

    @Override
    public boolean isMatchable(RestingOrder newRequest) {
        return newRequest.getQuantity() > 0 &&
                newRequest.getSide().equals(Side.BID) ?
                newRequest.getPrice() >= getPrice() :
                newRequest.getPrice() <= getPrice();
    }

    public void updateQuantityAndCreateTrade(long amount, double price) {
        this.quantity -= amount;
        Message execution = this.quantity == 0 ? new FilledOrder(newOrderRequest, price, amount) : new PartiallyFilledOrder(newOrderRequest, price, amount, this.quantity);
        executionListener.onNewExecution(execution);
    }

    /**
     * match the new request against this resting order. Changing quantity accordingly
     * @param newRequest the new request
     * @return true if there are no more quantity to fill in new request
     * (meaning this resting order can fully fill the new request and we
     * don't need to proceed to next resting order)
     */
    @Override
    public boolean fulfillNewRequest(RestingOrder newRequest) {
        long matchQuantity = Math.min(this.getQuantity(), newRequest.getQuantity());
        this.updateQuantityAndCreateTrade(matchQuantity, this.getPrice());
        newRequest.updateQuantityAndCreateTrade(matchQuantity, this.getPrice());
        return newRequest.getQuantity() == 0;
    }

    @Override
    public boolean isEmpty() {
        return getQuantity() == 0;
    }

    @Override
    public String toString() {
        return String.format("id=%d|time=%s|symbol=%s|side=%s|price=%f|quantity=%d", getId(), getTime(), getSymbol(), getSide(), getPrice(), getQuantity());
    }
}
