package com.baml.matchingengine.model;

import com.baml.matchingengine.data.Side;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by QuanNguyenHuu on 1/7/18.
 * DepthLevel implementation using linked list to store order of the same depth level
 */
public class LinkedOrderDepthLevel implements DepthLevel {

    private final double price;
    private final LinkedList<RestingOrder> orders = new LinkedList<>();

    public LinkedOrderDepthLevel(RestingOrder newRequest) {
        this.price = newRequest.getPrice();
        this.orders.add(newRequest);
    }

    public double getPrice() {
        return price;
    }

    public LinkedList<RestingOrder> getOrders() {
        return orders;
    }

    public void addOrder(RestingOrder newRequest) {
        orders.addLast(newRequest);
    }

    @Override
    public boolean isMatchable(RestingOrder newRequest) {
        return newRequest.getQuantity() > 0 &&
                newRequest.getSide().equals(Side.BID) ?
                newRequest.getPrice() >= getPrice() :
                newRequest.getPrice() <= getPrice();
    }

    /**
     * match the new request against this depth level. Changing quantity of
     * orders accordingly. Remove the order if quantity falls to 0.
     * @param newRequest the new request
     * @return true if there are no more quantity to fill in new request
     * (meaning this depth level can fully fill the new request and we
     * don't need to proceed to next depth level)
     */
    @Override
    public boolean fulfillNewRequest(RestingOrder newRequest) {
        Iterator<RestingOrder> iter = orders.iterator();
        while (iter.hasNext()) {
            RestingOrder next = iter.next();
            if (next.fulfillNewRequest(newRequest)) {
                if (next.isEmpty())
                    iter.remove();
                return true;
            }
            assert next.isEmpty();
            iter.remove();
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return orders.size() == 0;
    }

}
