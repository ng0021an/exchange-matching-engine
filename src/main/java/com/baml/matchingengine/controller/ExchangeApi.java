package com.baml.matchingengine.controller;

import com.baml.matchingengine.data.NewOrderRequest;
import com.baml.matchingengine.data.NewOrderRequestJson;
import com.baml.matchingengine.service.ExchangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by QuanNguyenHuu on 29/6/18.
 */
@RestController
public class ExchangeApi {

    Logger LOG = LoggerFactory.getLogger(ExchangeApi.class);

    private final ExchangeService exchangeService;

    @Autowired
    public ExchangeApi(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @PostMapping("/api/new")
    public String onNewOrderRequest(@RequestBody NewOrderRequestJson newOrderRequestJson) {
        LOG.info("Getting new order request {}", newOrderRequestJson);
        exchangeService.processNewOrderRequest(new NewOrderRequest(newOrderRequestJson));
        return "ok";
    }

    @GetMapping("/api/orderbook/{symbol}")
    public String onOrderBookRequest(@PathVariable("symbol") String symbol) {
        LOG.info("Getting initial image request {}");
        CompletableFuture<String> snapshot = new CompletableFuture<>();
        exchangeService.processOrderBookRequest(symbol, snapshot);
        try {
            return snapshot.get(1000, TimeUnit.MILLISECONDS);
        }
        catch (Exception e) {
            LOG.error("Error processing initial image request", e);
            //Can return more meaningful response here, but leave this for now
            return null;
        }
    }
}
