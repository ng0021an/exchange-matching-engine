package com.baml.matchingengine.data;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
public class NewOrderRequestJson {

    private String symbol;
    private String side;
    private double price;
    private long quantity;

    public NewOrderRequestJson() {}

    public NewOrderRequestJson(String symbol, String side, double price, long quantity) {
        this.symbol = symbol;
        this.side = side;
        this.price = price;
        this.quantity = quantity;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSide(String side) {
        if (side == null || !(side.equals("BID") || side.equals("ASK")))
            throw new RuntimeException("Invalid order side: " + side);
        this.side = side;
    }

    public String getSide() {
        return side;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getQuantity() {
        return quantity;
    }
}
