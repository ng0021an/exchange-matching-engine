package com.baml.matchingengine.data;

import java.util.concurrent.CompletableFuture;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
public class RequestEvent {

    public enum Type {
        REQUEST_NEW,
        ORDER_BOOK
    }

    private final Type type;
    private final Message message;
    private final CompletableFuture<String> result;

    public RequestEvent(Type type, Message message, CompletableFuture<String> result) {
        this.message = message;
        this.type = type;
        this.result = result;
    }

    public Message getMessage() {
        return message;
    }

    public Type getType() {
        return type;
    }

    public CompletableFuture<String> getResult() {
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s|%s", message, type);
    }
}
