package com.baml.matchingengine.data;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 * Order request
 */
public class NewOrderRequest extends Message {

    private static long nextId = 1;

    private final long id;

    //can also use UUID or date formatted string to ensure uniqueness;
    private final String symbol;
    private final Side side;
    private final double price;
    private final long quantity;

    protected NewOrderRequest(String symbol, Side side, double price, long quantity) {
        super(MessageType.REQUEST_NEW);
        this.id = nextId++;
        this.symbol = symbol;
        this.side = side;
        this.price = price;
        this.quantity = quantity;
    }

    public NewOrderRequest(NewOrderRequestJson newOrderRequestJson) {
        this(newOrderRequestJson.getSymbol(),
                Side.valueOf(newOrderRequestJson.getSide()),
                newOrderRequestJson.getPrice(),
                newOrderRequestJson.getQuantity()
        );
    }

    @Override
    public long getId() {
        return id;
    }

    public String getSymbol() {
        return symbol;
    }

    public Side getSide() {
        return side;
    }

    public double getPrice() {
        return price;
    }

    public long getQuantity() {
        return quantity;
    }

}
