package com.baml.matchingengine.data;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
public enum Side {
    BID,
    ASK;

    public Side getOpposite() {
        return this.equals(Side.BID) ? Side.ASK : Side.BID;
    }
}
