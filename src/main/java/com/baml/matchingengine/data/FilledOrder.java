package com.baml.matchingengine.data;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
public class FilledOrder extends Execution {

    private final double filledPrice;
    private final long filledQuantity;

    public FilledOrder(NewOrderRequest newOrderRequest, double filledPrice, long filledQuantity) {
        super(MessageType.FILLED, newOrderRequest);
        this.filledPrice = filledPrice;
        this.filledQuantity = filledQuantity;
    }

    public double getFilledPrice() {
        return filledPrice;
    }

    public long getFilledQuantity() {
        return filledQuantity;
    }

    @Override
    public String toString() {
        return String.format("%s|filledPrice=%f|filledQuantity=%d", super.toString(), getFilledPrice(), getFilledQuantity());
    }
}
