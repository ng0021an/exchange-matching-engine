package com.baml.matchingengine.data;

import java.time.ZonedDateTime;

/**
 * Created by QuanNguyenHuu on 29/6/18.
 * Normally we should always wrap symbol, price, quantity in a class instead of
 * using raw double, long, string. Leave for enhancement
 */
public abstract class Message {

    private final MessageType type;
    private final ZonedDateTime time;

    protected Message(MessageType type) {
        this.type = type;
        this.time = ZonedDateTime.now();
    }

    public MessageType getType() {
        return type;
    }

    public abstract long getId();

    public abstract String getSymbol();

    public abstract Side getSide();

    public abstract double getPrice();

    public abstract long getQuantity();

    public ZonedDateTime getTime() {
        return time;
    }

    /**
     * Naive way for serialisation, we could do JSON or FIX serialiser for enhancement
     * @return serialised message
     */
    public String serialise() {
        return toString();
    }

    @Override
    public String toString() {
        return String.format("type=%s|time=%s|symbol=%s|id=%d|side=%s|price=%f|quantity=%d",
                getType(), getTime(), getSymbol(), getId(), getSide(), getPrice(), getQuantity());
    }
}
