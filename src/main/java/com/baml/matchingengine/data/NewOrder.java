package com.baml.matchingengine.data;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 * Resting order
 */
public class NewOrder extends Execution {

    public NewOrder(NewOrderRequest newOrderRequest) {
        super(MessageType.NEW, newOrderRequest);
    }

}
