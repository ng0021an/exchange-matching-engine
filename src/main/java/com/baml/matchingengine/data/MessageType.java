package com.baml.matchingengine.data;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 * For simplicity, we ignore cancel, reject, replace message for now
 */
public enum MessageType {
    REQUEST_NEW,
    RESTING,
    NEW,
    FILLED,
    PARTIALLY_FILLED
}
