package com.baml.matchingengine.data;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
public class PartiallyFilledOrder extends Execution {

    private final double filledPrice;
    private final long filledQuantity;
    private final long remainingQuantity;

    public PartiallyFilledOrder(NewOrderRequest newOrderRequest, double filledPrice, long filledQuantity, long remainingQuantity) {
        super(MessageType.PARTIALLY_FILLED, newOrderRequest);
        this.filledPrice = filledPrice;
        this.filledQuantity = filledQuantity;
        this.remainingQuantity = remainingQuantity;
    }

    public double getFilledPrice() {
        return filledPrice;
    }

    public long getFilledQuantity() {
        return filledQuantity;
    }

    public long getRemainingQuantity() {
        return remainingQuantity;
    }

    @Override
    public String toString() {
        return String.format("%s|filledPrice=%f|filledQuantity=%d|remainingQuantity=%d",
                super.toString(), getFilledPrice(), getFilledQuantity(), getRemainingQuantity());
    }
}
