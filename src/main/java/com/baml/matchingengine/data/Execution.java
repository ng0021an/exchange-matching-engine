package com.baml.matchingengine.data;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
public class Execution extends Message {

    private static long nextExecId = 1;

    private final long execId;

    private final NewOrderRequest newOrderRequest;

    public Execution(MessageType messageType, NewOrderRequest newOrderRequest) {
        super(messageType);
        this.execId = nextExecId++;
        this.newOrderRequest = newOrderRequest;
    }

    @Override
    public long getId() {
        return newOrderRequest.getId();
    }

    @Override
    public String getSymbol() {
        return newOrderRequest.getSymbol();
    }

    @Override
    public Side getSide() {
        return newOrderRequest.getSide();
    }

    @Override
    public double getPrice() {
        return newOrderRequest.getPrice();
    }

    @Override
    public long getQuantity() {
        return newOrderRequest.getQuantity();
    }

    @Override
    public String toString() {
        return String.format("%s|execId=%d", super.toString(), execId);
    }

    public NewOrderRequest getNewOrderRequest() {
        return newOrderRequest;
    }
}
