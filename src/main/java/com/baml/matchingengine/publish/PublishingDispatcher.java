package com.baml.matchingengine.publish;

import com.baml.matchingengine.data.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
@Component
public class PublishingDispatcher implements ExecutionListener {

    Logger LOG = LoggerFactory.getLogger(PublishingDispatcher.class);

    private final BlockingQueue<Message> messages = new ArrayBlockingQueue<>(4000);
    private final List<Publisher> publishers;
    private volatile Thread thread;

    @Autowired
    public PublishingDispatcher(List<Publisher> publishers) {
        this.publishers = publishers;
    }

    @PostConstruct
    public void start() {
        thread = new Thread(() -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    Message message = messages.take();
                    List<Message> messagesToProcess = new ArrayList<>();
                    messagesToProcess.add(message);
                    messages.drainTo(messagesToProcess);
                    messagesToProcess.forEach(this::publish);
                }
            }
            catch (InterruptedException e) {
                LOG.warn("Publisher thread interrupted", e);
            }
        });
        thread.start();
    }

    @PreDestroy
    public void stop() {
        thread.interrupt();
        thread = null;
    }

    @Override
    public void onNewExecution(Message message) {
        try {
            //Need finer control over time out and queue size, leave later
            messages.put(message);
        }
        catch (InterruptedException e) {
            LOG.warn("Interrupted while publishing", e);
        }
    }

    private void publish(Message message) {
        publishers.forEach(publisher -> publisher.publish(message));
    }

}
