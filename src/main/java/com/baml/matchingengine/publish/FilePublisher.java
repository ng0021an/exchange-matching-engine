package com.baml.matchingengine.publish;

import com.baml.matchingengine.data.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
@Component
public class FilePublisher implements Publisher {

    Logger LOG = LoggerFactory.getLogger(FilePublisher.class);

    private PrintWriter requestWriter = null;
    private PrintWriter executionWriter = null;

    private ScheduledExecutorService executorService;

    @PostConstruct
    public void init() {
        try {
            this.requestWriter = new PrintWriter(Files.newBufferedWriter(Paths.get("./request.txt")));
            this.executionWriter = new PrintWriter(Files.newBufferedWriter(Paths.get("./execution.txt")));
        }
        catch (Exception e) {
            LOG.error("Error initializing File publisher", e);
            if (requestWriter != null) {
                try {
                    requestWriter.close();
                }
                catch (Exception ex) {
                    LOG.error("Error closing request writer", e);
                }
            }
            if (executionWriter != null) {
                try {
                    executionWriter.close();
                }
                catch (Exception ex) {
                    LOG.error("Error closing execution writer", e);
                }
            }
        }

        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this::flush, 0, 10, TimeUnit.MILLISECONDS);
    }

    @PreDestroy
    public void stop() {
        flush();
        executorService.shutdown();
    }

    @Override
    public void publish(Message message) {
        switch (message.getType()) {
            case REQUEST_NEW:
                requestWriter.println(message.serialise());
                break;
            case NEW:
            case FILLED:
            case PARTIALLY_FILLED:
                executionWriter.println(message.serialise());
                break;
            default:
                LOG.warn("Unknown message type for message {}", message);
        }
    }

    /**
     * writer is thread-safe so no need to synchronize
     */
    private void flush() {
        requestWriter.flush();
        executionWriter.flush();
    }
}
