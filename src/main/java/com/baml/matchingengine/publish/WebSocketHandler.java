package com.baml.matchingengine.publish;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by QuanNguyenHuu on 1/7/18.
 */
@Component
public class WebSocketHandler extends TextWebSocketHandler {

    Logger LOG = LoggerFactory.getLogger(WebSocketHandler.class);

    private final Queue<WebSocketSession> sessions = new ConcurrentLinkedQueue<>();
    private final Queue<WebSocketSession> subscribedSessions = new ConcurrentLinkedQueue<>();

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws InterruptedException, IOException {
        if (message.getPayload().equals("subscribe"))
            subscribedSessions.add(session);
        else if (message.getPayload().equals("unsubscribe"))
            subscribedSessions.remove(session);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        sessions.add(session);
    }

    @PreDestroy
    public void stop() {
        try {
            for (WebSocketSession session : sessions)
                session.close();
        }
        catch (IOException e) {
            LOG.error("Error closing web socket session", e);
        }
    }

    public void sendMessage(String message) throws IOException {
        for (WebSocketSession session : subscribedSessions) {
            if (session.isOpen())
                session.sendMessage(new TextMessage(message));
        }
    }
}
