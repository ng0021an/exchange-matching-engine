package com.baml.matchingengine.publish;

import com.baml.matchingengine.data.Message;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
public interface ExecutionListener {
    void onNewExecution(Message message);
}
