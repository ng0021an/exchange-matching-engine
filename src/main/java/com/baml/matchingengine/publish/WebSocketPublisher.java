package com.baml.matchingengine.publish;

import com.baml.matchingengine.data.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by QuanNguyenHuu on 1/7/18.
 */
@Component
public class WebSocketPublisher implements Publisher {

    Logger LOG = LoggerFactory.getLogger(WebSocketPublisher.class);

    private final WebSocketHandler webSocketHandler;

    @Autowired
    public WebSocketPublisher(WebSocketHandler webSocketHandler) {
        this.webSocketHandler = webSocketHandler;
    }

    @Override
    public void publish(Message message) {
        try {
            switch (message.getType()) {
                case REQUEST_NEW:
                    break;
                case NEW:
                case FILLED:
                case PARTIALLY_FILLED:
                    webSocketHandler.sendMessage(message.toString());
                    break;
                default:
                    LOG.warn("Unknown message type for message {}", message);
            }
        }
        catch (IOException e) {
            LOG.error("Error publishing message via web socket", e);
        }
    }
}
