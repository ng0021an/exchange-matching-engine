package com.baml.matchingengine.engine;

import com.baml.matchingengine.data.Side;
import com.baml.matchingengine.model.RestingOrder;
import com.baml.matchingengine.publish.ExecutionListener;

import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Created by QuanNguyenHuu on 1/7/18.
 * We could use a LinkedList for each Depth Level to improve removal performance when traversing
 * different levels for matching (since removing from linked list is O(1) and removing from
 * heap is O(logn). However naively doing that will increases the time to put new resting order to O(n)
 * since we need to find the level that the new order resides in the heap. Solution to that is
 * presented in AdvancedMatchingEngine.
 */
public class SimpleMatchingEngine extends AbstractMatchingEngine<RestingOrder> {

    public SimpleMatchingEngine(String symbol, ExecutionListener executionListener) {
        super(symbol, executionListener);
        this.orderBook.put(Side.ASK, new PriorityQueue<>(minHeapComparator));
        this.orderBook.put(Side.BID, new PriorityQueue<>(maxHeapComparator));
    }

    @Override
    protected void handleOrderBookRequest(CompletableFuture<String> result) {
        //Not the most performant way but whole snapshot orderbook request
        //should be less frequent so should be tolerable
        Set<RestingOrder> bids = new TreeSet<>(maxHeapComparator);
        bids.addAll(orderBook.get(Side.BID));
        Set<RestingOrder> asks = new TreeSet<>(minHeapComparator);
        asks.addAll(orderBook.get(Side.ASK));

        //Again can have more meaningful serializer but leave this for now
        result.complete(String.format("BID:%s,ASK:%s", bids, asks));
    }

    @Override
    protected void pushLeftOverLiquidityToStack(RestingOrder newRequest) {
        if (newRequest.getQuantity() > 0) {
            PriorityQueue<RestingOrder> currentStack = orderBook.get(newRequest.getSide());
            currentStack.add(newRequest);
        }
    }

    @Override
    protected void cleanUp(RestingOrder depthLevel) {
        //No clean up required for SimpleMatchingEngine
    }
}
