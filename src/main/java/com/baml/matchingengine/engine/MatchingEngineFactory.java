package com.baml.matchingengine.engine;

import com.baml.matchingengine.publish.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
@Component
public class MatchingEngineFactory {

    Logger LOG = LoggerFactory.getLogger(MatchingEngineFactory.class);

    private final ExecutionListener executionListener;

    @Autowired
    public MatchingEngineFactory(ExecutionListener executionListener) {
        this.executionListener = executionListener;
    }

    public MatchingEngineRunner createMatchingEngineRunner(String symbol) {
        LOG.info("Creating matching engine for symbol {}", symbol);
        return new MatchingEngineRunner(symbol, executionListener);
    }
}
