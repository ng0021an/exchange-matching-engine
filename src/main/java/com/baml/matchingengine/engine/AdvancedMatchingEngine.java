package com.baml.matchingengine.engine;

import com.baml.matchingengine.data.Side;
import com.baml.matchingengine.model.LinkedOrderDepthLevel;
import com.baml.matchingengine.model.RestingOrder;
import com.baml.matchingengine.publish.ExecutionListener;

import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Created by QuanNguyenHuu on 1/7/18.
 * Use linked list to store orders of the same depth level, which saves removing time
 * for order of the same depth level since removing first from linked list is O(1) while
 * removing top from heap is O(logn).
 * Use a Hashmap to store map from price to DepthLevel to optimize addition of a new order
 * to a depth level. This make worst case addition is O(logn) (a new level is created) and
 * best case addition is O(1) (found existing level in O(1) and add to linked list in O(1)).
 * Without this hash map addition will always be O(n) (find existing level in heap is O(n))
 */
public class AdvancedMatchingEngine extends AbstractMatchingEngine<LinkedOrderDepthLevel> {

    private final Comparator<LinkedOrderDepthLevel> minHeapLevelComparator = (level1, level2) -> {
        if (level1.getPrice() < level2.getPrice())
            return -1;
        if (level1.getPrice() > level2.getPrice())
            return 1;
        return 0;
    };

    private final Comparator<LinkedOrderDepthLevel> maxHeapLevelComparator = (level1, level2) -> {
        if (level1.getPrice() < level2.getPrice())
            return 1;
        if (level1.getPrice() > level2.getPrice())
            return -1;
        return 0;
    };

    private final Map<Double, LinkedOrderDepthLevel> priceToLevelMap = new HashMap<>();

    public AdvancedMatchingEngine(String symbol, ExecutionListener executionListener) {
        super(symbol, executionListener);
        this.orderBook.put(Side.ASK, new PriorityQueue<>(minHeapLevelComparator));
        this.orderBook.put(Side.BID, new PriorityQueue<>(maxHeapLevelComparator));
    }

    @Override
    protected void handleOrderBookRequest(CompletableFuture<String> result) {
        //Not the most performant way but whole snapshot orderbook request
        //should be less frequent so should be tolerable
        Set<RestingOrder> bids = new TreeSet<>(maxHeapComparator);
        orderBook.get(Side.BID).forEach(depthLevel -> bids.addAll(depthLevel.getOrders()));
        Set<RestingOrder> asks = new TreeSet<>(minHeapComparator);
        orderBook.get(Side.ASK).forEach(depthLevel -> asks.addAll(depthLevel.getOrders()));

        //Again can have more meaningful serializer but leave this for now
        result.complete(String.format("BID:%s,ASK:%s", bids, asks));
    }

    @Override
    protected void pushLeftOverLiquidityToStack(RestingOrder newRequest) {
        if (newRequest.getQuantity() > 0) {
            PriorityQueue<LinkedOrderDepthLevel> currentStack = orderBook.get(newRequest.getSide());
            LinkedOrderDepthLevel existingLevel = priceToLevelMap.get(newRequest.getPrice());
            if (existingLevel != null)
                existingLevel.addOrder(newRequest);
            else {
                LinkedOrderDepthLevel newLevel = new LinkedOrderDepthLevel(newRequest);
                priceToLevelMap.put(newRequest.getPrice(), newLevel);
                currentStack.add(newLevel);
            }
        }
    }

    @Override
    protected void cleanUp(LinkedOrderDepthLevel depthLevel) {
        priceToLevelMap.remove(depthLevel.getPrice());
    }
}
