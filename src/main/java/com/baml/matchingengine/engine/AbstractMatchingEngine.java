package com.baml.matchingengine.engine;

import com.baml.matchingengine.data.*;
import com.baml.matchingengine.model.DepthLevel;
import com.baml.matchingengine.model.RestingOrder;
import com.baml.matchingengine.publish.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 * One object per symbol. Use priority queue and non locking for optimal performance.
 * Non-locking is possible since this matcher is run on a single thread only.
 * There are no direct access from other threads to data used in this class (like orderBook).
 * This is possible since we convert all direct data access to events on blocking queue and
 * this thread is taking and processing events off the blocking queue.
 * Since the matching operation requires memory barrier for the whole duration and
 * the whole data, a multithreaded matcher will have worse performance since likely
 * we will need to lock the whole operation and the whole data, which is as good as
 * single thread plus the overhead of synchronizing.
 */
public abstract class AbstractMatchingEngine<T extends DepthLevel> {

    Logger LOG = LoggerFactory.getLogger(AbstractMatchingEngine.class);

    private final String symbol;
    private final ExecutionListener executionListener;

    //Use normal hashmap as this is run on single thread
    protected final Map<Side, PriorityQueue<T>> orderBook = new HashMap<>();

    protected final Comparator<RestingOrder> minHeapComparator = (order1, order2) -> {
        if (order1.getPrice() < order2.getPrice())
            return -1;
        if (order1.getPrice() > order2.getPrice())
            return 1;
        if (order1.getTime().isBefore(order2.getTime()))
            return -1;
        if (order1.getTime().isAfter(order2.getTime()))
            return 1;
        return 0;
    };

    protected final Comparator<RestingOrder> maxHeapComparator = (order1, order2) -> {
        if (order1.getPrice() < order2.getPrice())
            return 1;
        if (order1.getPrice() > order2.getPrice())
            return -1;
        if (order1.getTime().isBefore(order2.getTime()))
            return -1;
        if (order1.getTime().isAfter(order2.getTime()))
            return 1;
        return 0;
    };

    public AbstractMatchingEngine(String symbol, ExecutionListener executionListener) {
        this.symbol = symbol;
        this.executionListener = executionListener;
    }

    /**
     * No locking processing, possible thanks to running on single thread
     * @param requestEvent
     */
    public void onRequestEvent(RequestEvent requestEvent) {
        switch (requestEvent.getType()) {
            case REQUEST_NEW:
                handleNewOrderRequest(requestEvent.getMessage());
                break;
            case ORDER_BOOK:
                handleOrderBookRequest(requestEvent.getResult());
                break;
            default:
                LOG.warn("Unknown request type for {}", requestEvent);
        }

    }

    private void handleNewOrderRequest(Message message) {
        if (message instanceof NewOrderRequest) {
            RestingOrder newRequest = createRestingOrder(message);
            matchAgainstOppositeStack(newRequest);
            pushLeftOverLiquidityToStack(newRequest);
        }
        else
            LOG.info("Message type not handled for now {}", message);
    }

    private RestingOrder createRestingOrder(Message message) {
        RestingOrder newRequest = new RestingOrder((NewOrderRequest)message, executionListener);
        executionListener.onNewExecution(message);
        executionListener.onNewExecution(new NewOrder(newRequest.getNewOrderRequest()));
        return newRequest;
    }

    protected void matchAgainstOppositeStack(RestingOrder newRequest) {
        PriorityQueue<T> oppositeStack = orderBook.get(newRequest.getSide().getOpposite());

        T topOfBook = oppositeStack.peek();
        while (topOfBook != null && topOfBook.isMatchable(newRequest)) {
            if (topOfBook.fulfillNewRequest(newRequest)) {
                if (topOfBook.isEmpty())
                    removeTopOfBook(oppositeStack);
                return;
            }
            //Not fully filled by top of book, remove the empty top of book
            //and proceed to next resting order;
            assert topOfBook.isEmpty();
            removeTopOfBook(oppositeStack);
            topOfBook = oppositeStack.peek();
        }
    }

    private void removeTopOfBook(PriorityQueue<T> stack) {
        cleanUp(stack.remove());
    }

    protected abstract void handleOrderBookRequest(CompletableFuture<String> result);

    protected abstract void pushLeftOverLiquidityToStack(RestingOrder newRequest);

    protected abstract void cleanUp(T depthLevel);
}
