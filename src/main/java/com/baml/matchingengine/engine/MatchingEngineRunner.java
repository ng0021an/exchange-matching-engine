package com.baml.matchingengine.engine;

import com.baml.matchingengine.data.Message;
import com.baml.matchingengine.data.RequestEvent;
import com.baml.matchingengine.publish.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 * This class handle all processing for one symbol and run on a dedicated thread with little
 * blocking. The program will create one matching engine for one symbol so need to ensure
 * that number of symbols assign to this JVM is smaller than number of core to prevent starvation.
 * In an ideal case, we could run multiple JVM, each in charge of several symbols and then put
 * behind a load balancer for request routing.
 */
public class MatchingEngineRunner {

    Logger LOG = LoggerFactory.getLogger(MatchingEngineRunner.class);

    private final String symbol;
    private final ExecutionListener executionListener;

    private final BlockingQueue<RequestEvent> events = new ArrayBlockingQueue<>(4000);
    private final AbstractMatchingEngine matchingEngine;
    private volatile Thread thread;

    public MatchingEngineRunner(String symbol, ExecutionListener executionListener) {
        this.symbol = symbol;
        this.executionListener = executionListener;
        this.matchingEngine = new AdvancedMatchingEngine(symbol, executionListener);
    }

    public void startIfRequired() {
        if (thread == null) {
            thread = new Thread(() -> {
                try {
                    while (!Thread.currentThread().isInterrupted()) {
                        RequestEvent requestEvent = events.take();
                        List<RequestEvent> eventsToProcess = new ArrayList<>();
                        eventsToProcess.add(requestEvent);
                        events.drainTo(eventsToProcess);
                        eventsToProcess.forEach(matchingEngine::onRequestEvent);
                    }
                } catch (InterruptedException e) {
                    LOG.warn("Publisher thread interrupted", e);
                }
            });
            thread.start();
        }
        else
            LOG.info("Already started for symbol {}", symbol);
    }

    public void stopIfRequired() {
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }
        else
            LOG.info("Already stopped for symbol {}", symbol);
    }

    public void handleMessage(Message message) {
        //Using offer as request can be skipped if overloaded
        events.offer(new RequestEvent(RequestEvent.Type.REQUEST_NEW, message, null));
    }

    public void getOrderBook(CompletableFuture<String> snapshot) {
        events.offer(new RequestEvent(RequestEvent.Type.ORDER_BOOK, null, snapshot));
    }
}
