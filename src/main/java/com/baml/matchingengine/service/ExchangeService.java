package com.baml.matchingengine.service;

import com.baml.matchingengine.data.Message;
import com.baml.matchingengine.engine.MatchingEngineRunner;
import com.baml.matchingengine.engine.MatchingEngineFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
@Service
public class ExchangeService {

    private final MatchingEngineFactory engineFactory;
    //ConcurrentHashMap for better concurrency
    private final Map<String, MatchingEngineRunner> matchingEngineRunners = new ConcurrentHashMap<>();

    @Autowired
    public ExchangeService(MatchingEngineFactory engineFactory) {
        this.engineFactory = engineFactory;
    }

    @PreDestroy
    public void stop() {
        matchingEngineRunners.values().forEach(MatchingEngineRunner::stopIfRequired);
    }

    public void processNewOrderRequest(Message message) {
        //Never remove so should be thread safe without locking
        //Another way is eager init the map if we have the list of symbols beforehand,
        //then HashMap is sufficient
        MatchingEngineRunner matchingEngineRunner = matchingEngineRunners.computeIfAbsent(
                message.getSymbol(),
                engineFactory::createMatchingEngineRunner
        );
        matchingEngineRunner.startIfRequired();
        matchingEngineRunner.handleMessage(message);
    }

    public void processOrderBookRequest(String symbol, CompletableFuture<String> snapshot) {
        MatchingEngineRunner runner = matchingEngineRunners.get(symbol);
        if (runner == null)
            snapshot.complete(null);
        else
            runner.getOrderBook(snapshot);
    }
}
