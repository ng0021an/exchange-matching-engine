Feature: Matching Engine
  As a user
  I want to get the correct snapshot of the order book and the correct execution updates

  Scenario: No trade created
    Given I send order requests
      |symbol|side|price|quantity|
      |EURUSD|ASK |1.33 |7000    |
      |EURUSD|ASK |1.33 |5000    |
      |EURUSD|ASK |1.33 |1000    |
      |EURUSD|ASK |1.26 |6000    |
      |EURUSD|ASK |1.26 |4000    |
      |EURUSD|ASK |1.26 |1000    |
      |EURUSD|ASK |1.24 |3000    |
      |EURUSD|ASK |1.24 |2000    |
      |EURUSD|ASK |1.24 |1000    |
      |EURUSD|BID |1.22 |1000    |
      |EURUSD|BID |1.22 |3000    |
      |EURUSD|BID |1.22 |7000    |
      |EURUSD|BID |1.20 |1000    |
      |EURUSD|BID |1.20 |4000    |
      |EURUSD|BID |1.20 |8000    |
      |EURUSD|BID |1.17 |3000    |
      |EURUSD|BID |1.17 |5000    |
      |EURUSD|BID |1.17 |6000    |
      |GBPUSD|ASK |1.35 |7000    |
      |GBPUSD|ASK |1.35 |5000    |
      |GBPUSD|ASK |1.35 |1000    |
      |GBPUSD|ASK |1.28 |6000    |
      |GBPUSD|ASK |1.28 |4000    |
      |GBPUSD|ASK |1.28 |1000    |
      |GBPUSD|ASK |1.25 |3000    |
      |GBPUSD|ASK |1.25 |2000    |
      |GBPUSD|ASK |1.25 |1000    |
      |GBPUSD|BID |1.24 |1000    |
      |GBPUSD|BID |1.24 |3000    |
      |GBPUSD|BID |1.24 |7000    |
      |GBPUSD|BID |1.23 |1000    |
      |GBPUSD|BID |1.23 |4000    |
      |GBPUSD|BID |1.23 |8000    |
      |GBPUSD|BID |1.15 |3000    |
      |GBPUSD|BID |1.15 |5000    |
      |GBPUSD|BID |1.15 |6000    |

    When I get snapshot for EURUSD
    Then I should get snapshot as BID:[id=10||symbol=EURUSD|side=BID|price=1.220000|quantity=1000, id=11||symbol=EURUSD|side=BID|price=1.220000|quantity=3000, id=12||symbol=EURUSD|side=BID|price=1.220000|quantity=7000, id=13||symbol=EURUSD|side=BID|price=1.200000|quantity=1000, id=14||symbol=EURUSD|side=BID|price=1.200000|quantity=4000, id=15||symbol=EURUSD|side=BID|price=1.200000|quantity=8000, id=16||symbol=EURUSD|side=BID|price=1.170000|quantity=3000, id=17||symbol=EURUSD|side=BID|price=1.170000|quantity=5000, id=18||symbol=EURUSD|side=BID|price=1.170000|quantity=6000],ASK:[id=7||symbol=EURUSD|side=ASK|price=1.240000|quantity=3000, id=8||symbol=EURUSD|side=ASK|price=1.240000|quantity=2000, id=9||symbol=EURUSD|side=ASK|price=1.240000|quantity=1000, id=4||symbol=EURUSD|side=ASK|price=1.260000|quantity=6000, id=5||symbol=EURUSD|side=ASK|price=1.260000|quantity=4000, id=6||symbol=EURUSD|side=ASK|price=1.260000|quantity=1000, id=1||symbol=EURUSD|side=ASK|price=1.330000|quantity=7000, id=2||symbol=EURUSD|side=ASK|price=1.330000|quantity=5000, id=3||symbol=EURUSD|side=ASK|price=1.330000|quantity=1000]

    When I get snapshot for GBPUSD
    Then I should get snapshot as BID:[id=28||symbol=GBPUSD|side=BID|price=1.240000|quantity=1000, id=29||symbol=GBPUSD|side=BID|price=1.240000|quantity=3000, id=30||symbol=GBPUSD|side=BID|price=1.240000|quantity=7000, id=31||symbol=GBPUSD|side=BID|price=1.230000|quantity=1000, id=32||symbol=GBPUSD|side=BID|price=1.230000|quantity=4000, id=33||symbol=GBPUSD|side=BID|price=1.230000|quantity=8000, id=34||symbol=GBPUSD|side=BID|price=1.150000|quantity=3000, id=35||symbol=GBPUSD|side=BID|price=1.150000|quantity=5000, id=36||symbol=GBPUSD|side=BID|price=1.150000|quantity=6000],ASK:[id=25||symbol=GBPUSD|side=ASK|price=1.250000|quantity=3000, id=26||symbol=GBPUSD|side=ASK|price=1.250000|quantity=2000, id=27||symbol=GBPUSD|side=ASK|price=1.250000|quantity=1000, id=22||symbol=GBPUSD|side=ASK|price=1.280000|quantity=6000, id=23||symbol=GBPUSD|side=ASK|price=1.280000|quantity=4000, id=24||symbol=GBPUSD|side=ASK|price=1.280000|quantity=1000, id=19||symbol=GBPUSD|side=ASK|price=1.350000|quantity=7000, id=20||symbol=GBPUSD|side=ASK|price=1.350000|quantity=5000, id=21||symbol=GBPUSD|side=ASK|price=1.350000|quantity=1000]

    When I get request file
    Then I should get request file as
    """
type=REQUEST_NEW||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000
type=REQUEST_NEW||symbol=EURUSD|id=2|side=ASK|price=1.330000|quantity=5000
type=REQUEST_NEW||symbol=EURUSD|id=3|side=ASK|price=1.330000|quantity=1000
type=REQUEST_NEW||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000
type=REQUEST_NEW||symbol=EURUSD|id=5|side=ASK|price=1.260000|quantity=4000
type=REQUEST_NEW||symbol=EURUSD|id=6|side=ASK|price=1.260000|quantity=1000
type=REQUEST_NEW||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000
type=REQUEST_NEW||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000
type=REQUEST_NEW||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000
type=REQUEST_NEW||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000
type=REQUEST_NEW||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000
type=REQUEST_NEW||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000
type=REQUEST_NEW||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000
type=REQUEST_NEW||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000
type=REQUEST_NEW||symbol=EURUSD|id=15|side=BID|price=1.200000|quantity=8000
type=REQUEST_NEW||symbol=EURUSD|id=16|side=BID|price=1.170000|quantity=3000
type=REQUEST_NEW||symbol=EURUSD|id=17|side=BID|price=1.170000|quantity=5000
type=REQUEST_NEW||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000
type=REQUEST_NEW||symbol=GBPUSD|id=19|side=ASK|price=1.350000|quantity=7000
type=REQUEST_NEW||symbol=GBPUSD|id=20|side=ASK|price=1.350000|quantity=5000
type=REQUEST_NEW||symbol=GBPUSD|id=21|side=ASK|price=1.350000|quantity=1000
type=REQUEST_NEW||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000
type=REQUEST_NEW||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000
type=REQUEST_NEW||symbol=GBPUSD|id=24|side=ASK|price=1.280000|quantity=1000
type=REQUEST_NEW||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000
type=REQUEST_NEW||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000
type=REQUEST_NEW||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000
type=REQUEST_NEW||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000
type=REQUEST_NEW||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000
type=REQUEST_NEW||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000
type=REQUEST_NEW||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000
type=REQUEST_NEW||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000
type=REQUEST_NEW||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000
type=REQUEST_NEW||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000
type=REQUEST_NEW||symbol=GBPUSD|id=35|side=BID|price=1.150000|quantity=5000
type=REQUEST_NEW||symbol=GBPUSD|id=36|side=BID|price=1.150000|quantity=6000

"""

    When I get execution file
    Then I should get execution file as
    """
type=NEW||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000|execId=1
type=NEW||symbol=EURUSD|id=2|side=ASK|price=1.330000|quantity=5000|execId=2
type=NEW||symbol=EURUSD|id=3|side=ASK|price=1.330000|quantity=1000|execId=3
type=NEW||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=4
type=NEW||symbol=EURUSD|id=5|side=ASK|price=1.260000|quantity=4000|execId=5
type=NEW||symbol=EURUSD|id=6|side=ASK|price=1.260000|quantity=1000|execId=6
type=NEW||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=7
type=NEW||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=8
type=NEW||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=9
type=NEW||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=10
type=NEW||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=11
type=NEW||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=12
type=NEW||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000|execId=13
type=NEW||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=14
type=NEW||symbol=EURUSD|id=15|side=BID|price=1.200000|quantity=8000|execId=15
type=NEW||symbol=EURUSD|id=16|side=BID|price=1.170000|quantity=3000|execId=16
type=NEW||symbol=EURUSD|id=17|side=BID|price=1.170000|quantity=5000|execId=17
type=NEW||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000|execId=18
type=NEW||symbol=GBPUSD|id=19|side=ASK|price=1.350000|quantity=7000|execId=19
type=NEW||symbol=GBPUSD|id=20|side=ASK|price=1.350000|quantity=5000|execId=20
type=NEW||symbol=GBPUSD|id=21|side=ASK|price=1.350000|quantity=1000|execId=21
type=NEW||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000|execId=22
type=NEW||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=23
type=NEW||symbol=GBPUSD|id=24|side=ASK|price=1.280000|quantity=1000|execId=24
type=NEW||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=25
type=NEW||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=26
type=NEW||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=27
type=NEW||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=28
type=NEW||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=29
type=NEW||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=30
type=NEW||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000|execId=31
type=NEW||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000|execId=32
type=NEW||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=33
type=NEW||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000|execId=34
type=NEW||symbol=GBPUSD|id=35|side=BID|price=1.150000|quantity=5000|execId=35
type=NEW||symbol=GBPUSD|id=36|side=BID|price=1.150000|quantity=6000|execId=36

    """


  Scenario: Fully match with portion of top of book
    Given I send order requests
      |symbol|side|price|quantity|
      |EURUSD|ASK |1.21 |4000    |
      |EURUSD|BID |1.24 |3000    |
      |GBPUSD|ASK |1.24 |4000    |
      |GBPUSD|BID |1.28 |3000    |

    When I get snapshot for EURUSD
    Then I should get snapshot as BID:[id=12||symbol=EURUSD|side=BID|price=1.220000|quantity=7000, id=13||symbol=EURUSD|side=BID|price=1.200000|quantity=1000, id=14||symbol=EURUSD|side=BID|price=1.200000|quantity=4000, id=15||symbol=EURUSD|side=BID|price=1.200000|quantity=8000, id=16||symbol=EURUSD|side=BID|price=1.170000|quantity=3000, id=17||symbol=EURUSD|side=BID|price=1.170000|quantity=5000, id=18||symbol=EURUSD|side=BID|price=1.170000|quantity=6000],ASK:[id=8||symbol=EURUSD|side=ASK|price=1.240000|quantity=2000, id=9||symbol=EURUSD|side=ASK|price=1.240000|quantity=1000, id=4||symbol=EURUSD|side=ASK|price=1.260000|quantity=6000, id=5||symbol=EURUSD|side=ASK|price=1.260000|quantity=4000, id=6||symbol=EURUSD|side=ASK|price=1.260000|quantity=1000, id=1||symbol=EURUSD|side=ASK|price=1.330000|quantity=7000, id=2||symbol=EURUSD|side=ASK|price=1.330000|quantity=5000, id=3||symbol=EURUSD|side=ASK|price=1.330000|quantity=1000]

    When I get snapshot for GBPUSD
    Then I should get snapshot as BID:[id=30||symbol=GBPUSD|side=BID|price=1.240000|quantity=7000, id=31||symbol=GBPUSD|side=BID|price=1.230000|quantity=1000, id=32||symbol=GBPUSD|side=BID|price=1.230000|quantity=4000, id=33||symbol=GBPUSD|side=BID|price=1.230000|quantity=8000, id=34||symbol=GBPUSD|side=BID|price=1.150000|quantity=3000, id=35||symbol=GBPUSD|side=BID|price=1.150000|quantity=5000, id=36||symbol=GBPUSD|side=BID|price=1.150000|quantity=6000],ASK:[id=26||symbol=GBPUSD|side=ASK|price=1.250000|quantity=2000, id=27||symbol=GBPUSD|side=ASK|price=1.250000|quantity=1000, id=22||symbol=GBPUSD|side=ASK|price=1.280000|quantity=6000, id=23||symbol=GBPUSD|side=ASK|price=1.280000|quantity=4000, id=24||symbol=GBPUSD|side=ASK|price=1.280000|quantity=1000, id=19||symbol=GBPUSD|side=ASK|price=1.350000|quantity=7000, id=20||symbol=GBPUSD|side=ASK|price=1.350000|quantity=5000, id=21||symbol=GBPUSD|side=ASK|price=1.350000|quantity=1000]

    When I get execution file
    Then I should get execution file as
    """
type=NEW||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000|execId=1
type=NEW||symbol=EURUSD|id=2|side=ASK|price=1.330000|quantity=5000|execId=2
type=NEW||symbol=EURUSD|id=3|side=ASK|price=1.330000|quantity=1000|execId=3
type=NEW||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=4
type=NEW||symbol=EURUSD|id=5|side=ASK|price=1.260000|quantity=4000|execId=5
type=NEW||symbol=EURUSD|id=6|side=ASK|price=1.260000|quantity=1000|execId=6
type=NEW||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=7
type=NEW||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=8
type=NEW||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=9
type=NEW||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=10
type=NEW||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=11
type=NEW||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=12
type=NEW||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000|execId=13
type=NEW||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=14
type=NEW||symbol=EURUSD|id=15|side=BID|price=1.200000|quantity=8000|execId=15
type=NEW||symbol=EURUSD|id=16|side=BID|price=1.170000|quantity=3000|execId=16
type=NEW||symbol=EURUSD|id=17|side=BID|price=1.170000|quantity=5000|execId=17
type=NEW||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000|execId=18
type=NEW||symbol=GBPUSD|id=19|side=ASK|price=1.350000|quantity=7000|execId=19
type=NEW||symbol=GBPUSD|id=20|side=ASK|price=1.350000|quantity=5000|execId=20
type=NEW||symbol=GBPUSD|id=21|side=ASK|price=1.350000|quantity=1000|execId=21
type=NEW||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000|execId=22
type=NEW||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=23
type=NEW||symbol=GBPUSD|id=24|side=ASK|price=1.280000|quantity=1000|execId=24
type=NEW||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=25
type=NEW||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=26
type=NEW||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=27
type=NEW||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=28
type=NEW||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=29
type=NEW||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=30
type=NEW||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000|execId=31
type=NEW||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000|execId=32
type=NEW||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=33
type=NEW||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000|execId=34
type=NEW||symbol=GBPUSD|id=35|side=BID|price=1.150000|quantity=5000|execId=35
type=NEW||symbol=GBPUSD|id=36|side=BID|price=1.150000|quantity=6000|execId=36
type=NEW||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=37
type=FILLED||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=38|filledPrice=1.220000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=39|filledPrice=1.220000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=40|filledPrice=1.220000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=41|filledPrice=1.220000|filledQuantity=3000
type=NEW||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=42
type=FILLED||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=43|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=44|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=45
type=FILLED||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=46|filledPrice=1.240000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=47|filledPrice=1.240000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=48|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=49|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=50
type=FILLED||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=51|filledPrice=1.250000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=52|filledPrice=1.250000|filledQuantity=3000

"""

  Scenario: Fully match with remaining portion of top of book
    Given I send order requests
      |symbol|side|price|quantity|
      |EURUSD|ASK |1.22 |7000    |
      |EURUSD|BID |1.28 |3000    |
      |GBPUSD|ASK |1.23 |7000    |
      |GBPUSD|BID |1.26 |3000    |

    When I get snapshot for EURUSD
    Then I should get snapshot as BID:[id=13||symbol=EURUSD|side=BID|price=1.200000|quantity=1000, id=14||symbol=EURUSD|side=BID|price=1.200000|quantity=4000, id=15||symbol=EURUSD|side=BID|price=1.200000|quantity=8000, id=16||symbol=EURUSD|side=BID|price=1.170000|quantity=3000, id=17||symbol=EURUSD|side=BID|price=1.170000|quantity=5000, id=18||symbol=EURUSD|side=BID|price=1.170000|quantity=6000],ASK:[id=4||symbol=EURUSD|side=ASK|price=1.260000|quantity=6000, id=5||symbol=EURUSD|side=ASK|price=1.260000|quantity=4000, id=6||symbol=EURUSD|side=ASK|price=1.260000|quantity=1000, id=1||symbol=EURUSD|side=ASK|price=1.330000|quantity=7000, id=2||symbol=EURUSD|side=ASK|price=1.330000|quantity=5000, id=3||symbol=EURUSD|side=ASK|price=1.330000|quantity=1000]

    When I get snapshot for GBPUSD
    Then I should get snapshot as BID:[id=31||symbol=GBPUSD|side=BID|price=1.230000|quantity=1000, id=32||symbol=GBPUSD|side=BID|price=1.230000|quantity=4000, id=33||symbol=GBPUSD|side=BID|price=1.230000|quantity=8000, id=34||symbol=GBPUSD|side=BID|price=1.150000|quantity=3000, id=35||symbol=GBPUSD|side=BID|price=1.150000|quantity=5000, id=36||symbol=GBPUSD|side=BID|price=1.150000|quantity=6000],ASK:[id=22||symbol=GBPUSD|side=ASK|price=1.280000|quantity=6000, id=23||symbol=GBPUSD|side=ASK|price=1.280000|quantity=4000, id=24||symbol=GBPUSD|side=ASK|price=1.280000|quantity=1000, id=19||symbol=GBPUSD|side=ASK|price=1.350000|quantity=7000, id=20||symbol=GBPUSD|side=ASK|price=1.350000|quantity=5000, id=21||symbol=GBPUSD|side=ASK|price=1.350000|quantity=1000]

    When I get execution file
    Then I should get execution file as
    """
type=NEW||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000|execId=1
type=NEW||symbol=EURUSD|id=2|side=ASK|price=1.330000|quantity=5000|execId=2
type=NEW||symbol=EURUSD|id=3|side=ASK|price=1.330000|quantity=1000|execId=3
type=NEW||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=4
type=NEW||symbol=EURUSD|id=5|side=ASK|price=1.260000|quantity=4000|execId=5
type=NEW||symbol=EURUSD|id=6|side=ASK|price=1.260000|quantity=1000|execId=6
type=NEW||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=7
type=NEW||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=8
type=NEW||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=9
type=NEW||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=10
type=NEW||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=11
type=NEW||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=12
type=NEW||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000|execId=13
type=NEW||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=14
type=NEW||symbol=EURUSD|id=15|side=BID|price=1.200000|quantity=8000|execId=15
type=NEW||symbol=EURUSD|id=16|side=BID|price=1.170000|quantity=3000|execId=16
type=NEW||symbol=EURUSD|id=17|side=BID|price=1.170000|quantity=5000|execId=17
type=NEW||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000|execId=18
type=NEW||symbol=GBPUSD|id=19|side=ASK|price=1.350000|quantity=7000|execId=19
type=NEW||symbol=GBPUSD|id=20|side=ASK|price=1.350000|quantity=5000|execId=20
type=NEW||symbol=GBPUSD|id=21|side=ASK|price=1.350000|quantity=1000|execId=21
type=NEW||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000|execId=22
type=NEW||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=23
type=NEW||symbol=GBPUSD|id=24|side=ASK|price=1.280000|quantity=1000|execId=24
type=NEW||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=25
type=NEW||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=26
type=NEW||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=27
type=NEW||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=28
type=NEW||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=29
type=NEW||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=30
type=NEW||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000|execId=31
type=NEW||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000|execId=32
type=NEW||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=33
type=NEW||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000|execId=34
type=NEW||symbol=GBPUSD|id=35|side=BID|price=1.150000|quantity=5000|execId=35
type=NEW||symbol=GBPUSD|id=36|side=BID|price=1.150000|quantity=6000|execId=36
type=NEW||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=37
type=FILLED||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=38|filledPrice=1.220000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=39|filledPrice=1.220000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=40|filledPrice=1.220000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=41|filledPrice=1.220000|filledQuantity=3000
type=NEW||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=42
type=FILLED||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=43|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=44|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=45
type=FILLED||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=46|filledPrice=1.240000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=47|filledPrice=1.240000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=48|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=49|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=50
type=FILLED||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=51|filledPrice=1.250000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=52|filledPrice=1.250000|filledQuantity=3000
type=NEW||symbol=EURUSD|id=41|side=ASK|price=1.220000|quantity=7000|execId=53
type=FILLED||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=54|filledPrice=1.220000|filledQuantity=7000
type=FILLED||symbol=EURUSD|id=41|side=ASK|price=1.220000|quantity=7000|execId=55|filledPrice=1.220000|filledQuantity=7000
type=NEW||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=56
type=FILLED||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=57|filledPrice=1.240000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=58|filledPrice=1.240000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=59|filledPrice=1.240000|filledQuantity=1000
type=FILLED||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=60|filledPrice=1.240000|filledQuantity=1000
type=NEW||symbol=GBPUSD|id=43|side=ASK|price=1.230000|quantity=7000|execId=61
type=FILLED||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=62|filledPrice=1.240000|filledQuantity=7000
type=FILLED||symbol=GBPUSD|id=43|side=ASK|price=1.230000|quantity=7000|execId=63|filledPrice=1.240000|filledQuantity=7000
type=NEW||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=64
type=FILLED||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=65|filledPrice=1.250000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=66|filledPrice=1.250000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=67|filledPrice=1.250000|filledQuantity=1000
type=FILLED||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=68|filledPrice=1.250000|filledQuantity=1000

"""

  Scenario: Partially match with portion of top of book
    Given I send order requests
      |symbol|side|price|quantity|
      |EURUSD|ASK |1.16 |3000    |
      |EURUSD|BID |1.29 |3000    |
      |GBPUSD|ASK |1.14 |7000    |
      |GBPUSD|BID |1.28 |8000    |

    When I get snapshot for EURUSD
    Then I should get snapshot as BID:[id=14||symbol=EURUSD|side=BID|price=1.200000|quantity=2000, id=15||symbol=EURUSD|side=BID|price=1.200000|quantity=8000, id=16||symbol=EURUSD|side=BID|price=1.170000|quantity=3000, id=17||symbol=EURUSD|side=BID|price=1.170000|quantity=5000, id=18||symbol=EURUSD|side=BID|price=1.170000|quantity=6000],ASK:[id=4||symbol=EURUSD|side=ASK|price=1.260000|quantity=3000, id=5||symbol=EURUSD|side=ASK|price=1.260000|quantity=4000, id=6||symbol=EURUSD|side=ASK|price=1.260000|quantity=1000, id=1||symbol=EURUSD|side=ASK|price=1.330000|quantity=7000, id=2||symbol=EURUSD|side=ASK|price=1.330000|quantity=5000, id=3||symbol=EURUSD|side=ASK|price=1.330000|quantity=1000]

    When I get snapshot for GBPUSD
    Then I should get snapshot as BID:[id=33||symbol=GBPUSD|side=BID|price=1.230000|quantity=6000, id=34||symbol=GBPUSD|side=BID|price=1.150000|quantity=3000, id=35||symbol=GBPUSD|side=BID|price=1.150000|quantity=5000, id=36||symbol=GBPUSD|side=BID|price=1.150000|quantity=6000],ASK:[id=23||symbol=GBPUSD|side=ASK|price=1.280000|quantity=2000, id=24||symbol=GBPUSD|side=ASK|price=1.280000|quantity=1000, id=19||symbol=GBPUSD|side=ASK|price=1.350000|quantity=7000, id=20||symbol=GBPUSD|side=ASK|price=1.350000|quantity=5000, id=21||symbol=GBPUSD|side=ASK|price=1.350000|quantity=1000]

    When I get execution file
    Then I should get execution file as
    """
type=NEW||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000|execId=1
type=NEW||symbol=EURUSD|id=2|side=ASK|price=1.330000|quantity=5000|execId=2
type=NEW||symbol=EURUSD|id=3|side=ASK|price=1.330000|quantity=1000|execId=3
type=NEW||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=4
type=NEW||symbol=EURUSD|id=5|side=ASK|price=1.260000|quantity=4000|execId=5
type=NEW||symbol=EURUSD|id=6|side=ASK|price=1.260000|quantity=1000|execId=6
type=NEW||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=7
type=NEW||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=8
type=NEW||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=9
type=NEW||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=10
type=NEW||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=11
type=NEW||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=12
type=NEW||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000|execId=13
type=NEW||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=14
type=NEW||symbol=EURUSD|id=15|side=BID|price=1.200000|quantity=8000|execId=15
type=NEW||symbol=EURUSD|id=16|side=BID|price=1.170000|quantity=3000|execId=16
type=NEW||symbol=EURUSD|id=17|side=BID|price=1.170000|quantity=5000|execId=17
type=NEW||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000|execId=18
type=NEW||symbol=GBPUSD|id=19|side=ASK|price=1.350000|quantity=7000|execId=19
type=NEW||symbol=GBPUSD|id=20|side=ASK|price=1.350000|quantity=5000|execId=20
type=NEW||symbol=GBPUSD|id=21|side=ASK|price=1.350000|quantity=1000|execId=21
type=NEW||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000|execId=22
type=NEW||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=23
type=NEW||symbol=GBPUSD|id=24|side=ASK|price=1.280000|quantity=1000|execId=24
type=NEW||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=25
type=NEW||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=26
type=NEW||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=27
type=NEW||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=28
type=NEW||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=29
type=NEW||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=30
type=NEW||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000|execId=31
type=NEW||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000|execId=32
type=NEW||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=33
type=NEW||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000|execId=34
type=NEW||symbol=GBPUSD|id=35|side=BID|price=1.150000|quantity=5000|execId=35
type=NEW||symbol=GBPUSD|id=36|side=BID|price=1.150000|quantity=6000|execId=36
type=NEW||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=37
type=FILLED||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=38|filledPrice=1.220000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=39|filledPrice=1.220000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=40|filledPrice=1.220000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=41|filledPrice=1.220000|filledQuantity=3000
type=NEW||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=42
type=FILLED||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=43|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=44|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=45
type=FILLED||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=46|filledPrice=1.240000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=47|filledPrice=1.240000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=48|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=49|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=50
type=FILLED||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=51|filledPrice=1.250000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=52|filledPrice=1.250000|filledQuantity=3000
type=NEW||symbol=EURUSD|id=41|side=ASK|price=1.220000|quantity=7000|execId=53
type=FILLED||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=54|filledPrice=1.220000|filledQuantity=7000
type=FILLED||symbol=EURUSD|id=41|side=ASK|price=1.220000|quantity=7000|execId=55|filledPrice=1.220000|filledQuantity=7000
type=NEW||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=56
type=FILLED||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=57|filledPrice=1.240000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=58|filledPrice=1.240000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=59|filledPrice=1.240000|filledQuantity=1000
type=FILLED||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=60|filledPrice=1.240000|filledQuantity=1000
type=NEW||symbol=GBPUSD|id=43|side=ASK|price=1.230000|quantity=7000|execId=61
type=FILLED||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=62|filledPrice=1.240000|filledQuantity=7000
type=FILLED||symbol=GBPUSD|id=43|side=ASK|price=1.230000|quantity=7000|execId=63|filledPrice=1.240000|filledQuantity=7000
type=NEW||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=64
type=FILLED||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=65|filledPrice=1.250000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=66|filledPrice=1.250000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=67|filledPrice=1.250000|filledQuantity=1000
type=FILLED||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=68|filledPrice=1.250000|filledQuantity=1000
type=NEW||symbol=EURUSD|id=45|side=ASK|price=1.160000|quantity=3000|execId=69
type=FILLED||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000|execId=70|filledPrice=1.200000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=45|side=ASK|price=1.160000|quantity=3000|execId=71|filledPrice=1.200000|filledQuantity=1000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=72|filledPrice=1.200000|filledQuantity=2000|remainingQuantity=2000
type=FILLED||symbol=EURUSD|id=45|side=ASK|price=1.160000|quantity=3000|execId=73|filledPrice=1.200000|filledQuantity=2000
type=NEW||symbol=EURUSD|id=46|side=BID|price=1.290000|quantity=3000|execId=74
type=PARTIALLY_FILLED||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=75|filledPrice=1.260000|filledQuantity=3000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=46|side=BID|price=1.290000|quantity=3000|execId=76|filledPrice=1.260000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=77
type=FILLED||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000|execId=78|filledPrice=1.230000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=79|filledPrice=1.230000|filledQuantity=1000|remainingQuantity=6000
type=FILLED||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000|execId=80|filledPrice=1.230000|filledQuantity=4000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=81|filledPrice=1.230000|filledQuantity=4000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=82|filledPrice=1.230000|filledQuantity=2000|remainingQuantity=6000
type=FILLED||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=83|filledPrice=1.230000|filledQuantity=2000
type=NEW||symbol=GBPUSD|id=48|side=BID|price=1.280000|quantity=8000|execId=84
type=FILLED||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000|execId=85|filledPrice=1.280000|filledQuantity=6000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=48|side=BID|price=1.280000|quantity=8000|execId=86|filledPrice=1.280000|filledQuantity=6000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=87|filledPrice=1.280000|filledQuantity=2000|remainingQuantity=2000
type=FILLED||symbol=GBPUSD|id=48|side=BID|price=1.280000|quantity=8000|execId=88|filledPrice=1.280000|filledQuantity=2000

"""

  Scenario: Fully match with remaining portion of top of book and partially match with portion of depth level 2
    Given I send order requests
      |symbol|side|price|quantity|
      |EURUSD|ASK |1.17 |19000   |
      |EURUSD|BID |1.33 |10000   |
      |GBPUSD|ASK |1.15 |8000    |
      |GBPUSD|BID |1.35 |15000   |

    When I get snapshot for EURUSD
    Then I should get snapshot as BID:[id=18||symbol=EURUSD|side=BID|price=1.170000|quantity=5000],ASK:[id=1||symbol=EURUSD|side=ASK|price=1.330000|quantity=5000, id=2||symbol=EURUSD|side=ASK|price=1.330000|quantity=5000, id=3||symbol=EURUSD|side=ASK|price=1.330000|quantity=1000]

    When I get snapshot for GBPUSD
    Then I should get snapshot as BID:[id=34||symbol=GBPUSD|side=BID|price=1.150000|quantity=1000, id=35||symbol=GBPUSD|side=BID|price=1.150000|quantity=5000, id=36||symbol=GBPUSD|side=BID|price=1.150000|quantity=6000],ASK:[id=21||symbol=GBPUSD|side=ASK|price=1.350000|quantity=1000]

    When I get execution file
    Then I should get execution file as
    """
type=NEW||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000|execId=1
type=NEW||symbol=EURUSD|id=2|side=ASK|price=1.330000|quantity=5000|execId=2
type=NEW||symbol=EURUSD|id=3|side=ASK|price=1.330000|quantity=1000|execId=3
type=NEW||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=4
type=NEW||symbol=EURUSD|id=5|side=ASK|price=1.260000|quantity=4000|execId=5
type=NEW||symbol=EURUSD|id=6|side=ASK|price=1.260000|quantity=1000|execId=6
type=NEW||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=7
type=NEW||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=8
type=NEW||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=9
type=NEW||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=10
type=NEW||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=11
type=NEW||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=12
type=NEW||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000|execId=13
type=NEW||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=14
type=NEW||symbol=EURUSD|id=15|side=BID|price=1.200000|quantity=8000|execId=15
type=NEW||symbol=EURUSD|id=16|side=BID|price=1.170000|quantity=3000|execId=16
type=NEW||symbol=EURUSD|id=17|side=BID|price=1.170000|quantity=5000|execId=17
type=NEW||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000|execId=18
type=NEW||symbol=GBPUSD|id=19|side=ASK|price=1.350000|quantity=7000|execId=19
type=NEW||symbol=GBPUSD|id=20|side=ASK|price=1.350000|quantity=5000|execId=20
type=NEW||symbol=GBPUSD|id=21|side=ASK|price=1.350000|quantity=1000|execId=21
type=NEW||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000|execId=22
type=NEW||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=23
type=NEW||symbol=GBPUSD|id=24|side=ASK|price=1.280000|quantity=1000|execId=24
type=NEW||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=25
type=NEW||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=26
type=NEW||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=27
type=NEW||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=28
type=NEW||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=29
type=NEW||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=30
type=NEW||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000|execId=31
type=NEW||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000|execId=32
type=NEW||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=33
type=NEW||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000|execId=34
type=NEW||symbol=GBPUSD|id=35|side=BID|price=1.150000|quantity=5000|execId=35
type=NEW||symbol=GBPUSD|id=36|side=BID|price=1.150000|quantity=6000|execId=36
type=NEW||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=37
type=FILLED||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=38|filledPrice=1.220000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=39|filledPrice=1.220000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=40|filledPrice=1.220000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=41|filledPrice=1.220000|filledQuantity=3000
type=NEW||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=42
type=FILLED||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=43|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=44|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=45
type=FILLED||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=46|filledPrice=1.240000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=47|filledPrice=1.240000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=48|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=49|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=50
type=FILLED||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=51|filledPrice=1.250000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=52|filledPrice=1.250000|filledQuantity=3000
type=NEW||symbol=EURUSD|id=41|side=ASK|price=1.220000|quantity=7000|execId=53
type=FILLED||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=54|filledPrice=1.220000|filledQuantity=7000
type=FILLED||symbol=EURUSD|id=41|side=ASK|price=1.220000|quantity=7000|execId=55|filledPrice=1.220000|filledQuantity=7000
type=NEW||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=56
type=FILLED||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=57|filledPrice=1.240000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=58|filledPrice=1.240000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=59|filledPrice=1.240000|filledQuantity=1000
type=FILLED||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=60|filledPrice=1.240000|filledQuantity=1000
type=NEW||symbol=GBPUSD|id=43|side=ASK|price=1.230000|quantity=7000|execId=61
type=FILLED||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=62|filledPrice=1.240000|filledQuantity=7000
type=FILLED||symbol=GBPUSD|id=43|side=ASK|price=1.230000|quantity=7000|execId=63|filledPrice=1.240000|filledQuantity=7000
type=NEW||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=64
type=FILLED||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=65|filledPrice=1.250000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=66|filledPrice=1.250000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=67|filledPrice=1.250000|filledQuantity=1000
type=FILLED||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=68|filledPrice=1.250000|filledQuantity=1000
type=NEW||symbol=EURUSD|id=45|side=ASK|price=1.160000|quantity=3000|execId=69
type=FILLED||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000|execId=70|filledPrice=1.200000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=45|side=ASK|price=1.160000|quantity=3000|execId=71|filledPrice=1.200000|filledQuantity=1000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=72|filledPrice=1.200000|filledQuantity=2000|remainingQuantity=2000
type=FILLED||symbol=EURUSD|id=45|side=ASK|price=1.160000|quantity=3000|execId=73|filledPrice=1.200000|filledQuantity=2000
type=NEW||symbol=EURUSD|id=46|side=BID|price=1.290000|quantity=3000|execId=74
type=PARTIALLY_FILLED||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=75|filledPrice=1.260000|filledQuantity=3000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=46|side=BID|price=1.290000|quantity=3000|execId=76|filledPrice=1.260000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=77
type=FILLED||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000|execId=78|filledPrice=1.230000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=79|filledPrice=1.230000|filledQuantity=1000|remainingQuantity=6000
type=FILLED||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000|execId=80|filledPrice=1.230000|filledQuantity=4000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=81|filledPrice=1.230000|filledQuantity=4000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=82|filledPrice=1.230000|filledQuantity=2000|remainingQuantity=6000
type=FILLED||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=83|filledPrice=1.230000|filledQuantity=2000
type=NEW||symbol=GBPUSD|id=48|side=BID|price=1.280000|quantity=8000|execId=84
type=FILLED||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000|execId=85|filledPrice=1.280000|filledQuantity=6000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=48|side=BID|price=1.280000|quantity=8000|execId=86|filledPrice=1.280000|filledQuantity=6000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=87|filledPrice=1.280000|filledQuantity=2000|remainingQuantity=2000
type=FILLED||symbol=GBPUSD|id=48|side=BID|price=1.280000|quantity=8000|execId=88|filledPrice=1.280000|filledQuantity=2000
type=NEW||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=89
type=FILLED||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=90|filledPrice=1.200000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=91|filledPrice=1.200000|filledQuantity=2000|remainingQuantity=17000
type=FILLED||symbol=EURUSD|id=15|side=BID|price=1.200000|quantity=8000|execId=92|filledPrice=1.200000|filledQuantity=8000
type=PARTIALLY_FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=93|filledPrice=1.200000|filledQuantity=8000|remainingQuantity=9000
type=FILLED||symbol=EURUSD|id=16|side=BID|price=1.170000|quantity=3000|execId=94|filledPrice=1.170000|filledQuantity=3000
type=PARTIALLY_FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=95|filledPrice=1.170000|filledQuantity=3000|remainingQuantity=6000
type=FILLED||symbol=EURUSD|id=17|side=BID|price=1.170000|quantity=5000|execId=96|filledPrice=1.170000|filledQuantity=5000
type=PARTIALLY_FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=97|filledPrice=1.170000|filledQuantity=5000|remainingQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000|execId=98|filledPrice=1.170000|filledQuantity=1000|remainingQuantity=5000
type=FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=99|filledPrice=1.170000|filledQuantity=1000
type=NEW||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=100
type=FILLED||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=101|filledPrice=1.260000|filledQuantity=3000
type=PARTIALLY_FILLED||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=102|filledPrice=1.260000|filledQuantity=3000|remainingQuantity=7000
type=FILLED||symbol=EURUSD|id=5|side=ASK|price=1.260000|quantity=4000|execId=103|filledPrice=1.260000|filledQuantity=4000
type=PARTIALLY_FILLED||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=104|filledPrice=1.260000|filledQuantity=4000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=6|side=ASK|price=1.260000|quantity=1000|execId=105|filledPrice=1.260000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=106|filledPrice=1.260000|filledQuantity=1000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000|execId=107|filledPrice=1.330000|filledQuantity=2000|remainingQuantity=5000
type=FILLED||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=108|filledPrice=1.330000|filledQuantity=2000
type=NEW||symbol=GBPUSD|id=51|side=ASK|price=1.150000|quantity=8000|execId=109
type=FILLED||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=110|filledPrice=1.230000|filledQuantity=6000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=51|side=ASK|price=1.150000|quantity=8000|execId=111|filledPrice=1.230000|filledQuantity=6000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000|execId=112|filledPrice=1.150000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=GBPUSD|id=51|side=ASK|price=1.150000|quantity=8000|execId=113|filledPrice=1.150000|filledQuantity=2000
type=NEW||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=114
type=FILLED||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=115|filledPrice=1.280000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=116|filledPrice=1.280000|filledQuantity=2000|remainingQuantity=13000
type=FILLED||symbol=GBPUSD|id=24|side=ASK|price=1.280000|quantity=1000|execId=117|filledPrice=1.280000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=118|filledPrice=1.280000|filledQuantity=1000|remainingQuantity=12000
type=FILLED||symbol=GBPUSD|id=19|side=ASK|price=1.350000|quantity=7000|execId=119|filledPrice=1.350000|filledQuantity=7000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=120|filledPrice=1.350000|filledQuantity=7000|remainingQuantity=5000
type=FILLED||symbol=GBPUSD|id=20|side=ASK|price=1.350000|quantity=5000|execId=121|filledPrice=1.350000|filledQuantity=5000
type=FILLED||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=122|filledPrice=1.350000|filledQuantity=5000

"""

  Scenario: Fully match with remaining portion of order book and create extra liquidity
    Given I send order requests
      |symbol|side|price|quantity|
      |EURUSD|ASK |1.17 |10000   |
      |EURUSD|BID |1.33 |15000   |
      |GBPUSD|ASK |1.15 |14000   |
      |GBPUSD|BID |1.35 |5000    |

    When I get snapshot for EURUSD
    Then I should get snapshot as BID:[],ASK:[id=3||symbol=EURUSD|side=ASK|price=1.330000|quantity=1000]

    When I get snapshot for GBPUSD
    Then I should get snapshot as BID:[id=56||symbol=GBPUSD|side=BID|price=1.350000|quantity=2000],ASK:[]

    When I get execution file
    Then I should get execution file as
    """
type=NEW||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000|execId=1
type=NEW||symbol=EURUSD|id=2|side=ASK|price=1.330000|quantity=5000|execId=2
type=NEW||symbol=EURUSD|id=3|side=ASK|price=1.330000|quantity=1000|execId=3
type=NEW||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=4
type=NEW||symbol=EURUSD|id=5|side=ASK|price=1.260000|quantity=4000|execId=5
type=NEW||symbol=EURUSD|id=6|side=ASK|price=1.260000|quantity=1000|execId=6
type=NEW||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=7
type=NEW||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=8
type=NEW||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=9
type=NEW||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=10
type=NEW||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=11
type=NEW||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=12
type=NEW||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000|execId=13
type=NEW||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=14
type=NEW||symbol=EURUSD|id=15|side=BID|price=1.200000|quantity=8000|execId=15
type=NEW||symbol=EURUSD|id=16|side=BID|price=1.170000|quantity=3000|execId=16
type=NEW||symbol=EURUSD|id=17|side=BID|price=1.170000|quantity=5000|execId=17
type=NEW||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000|execId=18
type=NEW||symbol=GBPUSD|id=19|side=ASK|price=1.350000|quantity=7000|execId=19
type=NEW||symbol=GBPUSD|id=20|side=ASK|price=1.350000|quantity=5000|execId=20
type=NEW||symbol=GBPUSD|id=21|side=ASK|price=1.350000|quantity=1000|execId=21
type=NEW||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000|execId=22
type=NEW||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=23
type=NEW||symbol=GBPUSD|id=24|side=ASK|price=1.280000|quantity=1000|execId=24
type=NEW||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=25
type=NEW||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=26
type=NEW||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=27
type=NEW||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=28
type=NEW||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=29
type=NEW||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=30
type=NEW||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000|execId=31
type=NEW||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000|execId=32
type=NEW||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=33
type=NEW||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000|execId=34
type=NEW||symbol=GBPUSD|id=35|side=BID|price=1.150000|quantity=5000|execId=35
type=NEW||symbol=GBPUSD|id=36|side=BID|price=1.150000|quantity=6000|execId=36
type=NEW||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=37
type=FILLED||symbol=EURUSD|id=10|side=BID|price=1.220000|quantity=1000|execId=38|filledPrice=1.220000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=39|filledPrice=1.220000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=11|side=BID|price=1.220000|quantity=3000|execId=40|filledPrice=1.220000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=37|side=ASK|price=1.210000|quantity=4000|execId=41|filledPrice=1.220000|filledQuantity=3000
type=NEW||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=42
type=FILLED||symbol=EURUSD|id=7|side=ASK|price=1.240000|quantity=3000|execId=43|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=EURUSD|id=38|side=BID|price=1.240000|quantity=3000|execId=44|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=45
type=FILLED||symbol=GBPUSD|id=28|side=BID|price=1.240000|quantity=1000|execId=46|filledPrice=1.240000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=47|filledPrice=1.240000|filledQuantity=1000|remainingQuantity=3000
type=FILLED||symbol=GBPUSD|id=29|side=BID|price=1.240000|quantity=3000|execId=48|filledPrice=1.240000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=39|side=ASK|price=1.240000|quantity=4000|execId=49|filledPrice=1.240000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=50
type=FILLED||symbol=GBPUSD|id=25|side=ASK|price=1.250000|quantity=3000|execId=51|filledPrice=1.250000|filledQuantity=3000
type=FILLED||symbol=GBPUSD|id=40|side=BID|price=1.280000|quantity=3000|execId=52|filledPrice=1.250000|filledQuantity=3000
type=NEW||symbol=EURUSD|id=41|side=ASK|price=1.220000|quantity=7000|execId=53
type=FILLED||symbol=EURUSD|id=12|side=BID|price=1.220000|quantity=7000|execId=54|filledPrice=1.220000|filledQuantity=7000
type=FILLED||symbol=EURUSD|id=41|side=ASK|price=1.220000|quantity=7000|execId=55|filledPrice=1.220000|filledQuantity=7000
type=NEW||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=56
type=FILLED||symbol=EURUSD|id=8|side=ASK|price=1.240000|quantity=2000|execId=57|filledPrice=1.240000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=58|filledPrice=1.240000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=EURUSD|id=9|side=ASK|price=1.240000|quantity=1000|execId=59|filledPrice=1.240000|filledQuantity=1000
type=FILLED||symbol=EURUSD|id=42|side=BID|price=1.280000|quantity=3000|execId=60|filledPrice=1.240000|filledQuantity=1000
type=NEW||symbol=GBPUSD|id=43|side=ASK|price=1.230000|quantity=7000|execId=61
type=FILLED||symbol=GBPUSD|id=30|side=BID|price=1.240000|quantity=7000|execId=62|filledPrice=1.240000|filledQuantity=7000
type=FILLED||symbol=GBPUSD|id=43|side=ASK|price=1.230000|quantity=7000|execId=63|filledPrice=1.240000|filledQuantity=7000
type=NEW||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=64
type=FILLED||symbol=GBPUSD|id=26|side=ASK|price=1.250000|quantity=2000|execId=65|filledPrice=1.250000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=66|filledPrice=1.250000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=GBPUSD|id=27|side=ASK|price=1.250000|quantity=1000|execId=67|filledPrice=1.250000|filledQuantity=1000
type=FILLED||symbol=GBPUSD|id=44|side=BID|price=1.260000|quantity=3000|execId=68|filledPrice=1.250000|filledQuantity=1000
type=NEW||symbol=EURUSD|id=45|side=ASK|price=1.160000|quantity=3000|execId=69
type=FILLED||symbol=EURUSD|id=13|side=BID|price=1.200000|quantity=1000|execId=70|filledPrice=1.200000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=45|side=ASK|price=1.160000|quantity=3000|execId=71|filledPrice=1.200000|filledQuantity=1000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=72|filledPrice=1.200000|filledQuantity=2000|remainingQuantity=2000
type=FILLED||symbol=EURUSD|id=45|side=ASK|price=1.160000|quantity=3000|execId=73|filledPrice=1.200000|filledQuantity=2000
type=NEW||symbol=EURUSD|id=46|side=BID|price=1.290000|quantity=3000|execId=74
type=PARTIALLY_FILLED||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=75|filledPrice=1.260000|filledQuantity=3000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=46|side=BID|price=1.290000|quantity=3000|execId=76|filledPrice=1.260000|filledQuantity=3000
type=NEW||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=77
type=FILLED||symbol=GBPUSD|id=31|side=BID|price=1.230000|quantity=1000|execId=78|filledPrice=1.230000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=79|filledPrice=1.230000|filledQuantity=1000|remainingQuantity=6000
type=FILLED||symbol=GBPUSD|id=32|side=BID|price=1.230000|quantity=4000|execId=80|filledPrice=1.230000|filledQuantity=4000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=81|filledPrice=1.230000|filledQuantity=4000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=82|filledPrice=1.230000|filledQuantity=2000|remainingQuantity=6000
type=FILLED||symbol=GBPUSD|id=47|side=ASK|price=1.140000|quantity=7000|execId=83|filledPrice=1.230000|filledQuantity=2000
type=NEW||symbol=GBPUSD|id=48|side=BID|price=1.280000|quantity=8000|execId=84
type=FILLED||symbol=GBPUSD|id=22|side=ASK|price=1.280000|quantity=6000|execId=85|filledPrice=1.280000|filledQuantity=6000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=48|side=BID|price=1.280000|quantity=8000|execId=86|filledPrice=1.280000|filledQuantity=6000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=87|filledPrice=1.280000|filledQuantity=2000|remainingQuantity=2000
type=FILLED||symbol=GBPUSD|id=48|side=BID|price=1.280000|quantity=8000|execId=88|filledPrice=1.280000|filledQuantity=2000
type=NEW||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=89
type=FILLED||symbol=EURUSD|id=14|side=BID|price=1.200000|quantity=4000|execId=90|filledPrice=1.200000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=91|filledPrice=1.200000|filledQuantity=2000|remainingQuantity=17000
type=FILLED||symbol=EURUSD|id=15|side=BID|price=1.200000|quantity=8000|execId=92|filledPrice=1.200000|filledQuantity=8000
type=PARTIALLY_FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=93|filledPrice=1.200000|filledQuantity=8000|remainingQuantity=9000
type=FILLED||symbol=EURUSD|id=16|side=BID|price=1.170000|quantity=3000|execId=94|filledPrice=1.170000|filledQuantity=3000
type=PARTIALLY_FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=95|filledPrice=1.170000|filledQuantity=3000|remainingQuantity=6000
type=FILLED||symbol=EURUSD|id=17|side=BID|price=1.170000|quantity=5000|execId=96|filledPrice=1.170000|filledQuantity=5000
type=PARTIALLY_FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=97|filledPrice=1.170000|filledQuantity=5000|remainingQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000|execId=98|filledPrice=1.170000|filledQuantity=1000|remainingQuantity=5000
type=FILLED||symbol=EURUSD|id=49|side=ASK|price=1.170000|quantity=19000|execId=99|filledPrice=1.170000|filledQuantity=1000
type=NEW||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=100
type=FILLED||symbol=EURUSD|id=4|side=ASK|price=1.260000|quantity=6000|execId=101|filledPrice=1.260000|filledQuantity=3000
type=PARTIALLY_FILLED||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=102|filledPrice=1.260000|filledQuantity=3000|remainingQuantity=7000
type=FILLED||symbol=EURUSD|id=5|side=ASK|price=1.260000|quantity=4000|execId=103|filledPrice=1.260000|filledQuantity=4000
type=PARTIALLY_FILLED||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=104|filledPrice=1.260000|filledQuantity=4000|remainingQuantity=3000
type=FILLED||symbol=EURUSD|id=6|side=ASK|price=1.260000|quantity=1000|execId=105|filledPrice=1.260000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=106|filledPrice=1.260000|filledQuantity=1000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000|execId=107|filledPrice=1.330000|filledQuantity=2000|remainingQuantity=5000
type=FILLED||symbol=EURUSD|id=50|side=BID|price=1.330000|quantity=10000|execId=108|filledPrice=1.330000|filledQuantity=2000
type=NEW||symbol=GBPUSD|id=51|side=ASK|price=1.150000|quantity=8000|execId=109
type=FILLED||symbol=GBPUSD|id=33|side=BID|price=1.230000|quantity=8000|execId=110|filledPrice=1.230000|filledQuantity=6000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=51|side=ASK|price=1.150000|quantity=8000|execId=111|filledPrice=1.230000|filledQuantity=6000|remainingQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000|execId=112|filledPrice=1.150000|filledQuantity=2000|remainingQuantity=1000
type=FILLED||symbol=GBPUSD|id=51|side=ASK|price=1.150000|quantity=8000|execId=113|filledPrice=1.150000|filledQuantity=2000
type=NEW||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=114
type=FILLED||symbol=GBPUSD|id=23|side=ASK|price=1.280000|quantity=4000|execId=115|filledPrice=1.280000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=116|filledPrice=1.280000|filledQuantity=2000|remainingQuantity=13000
type=FILLED||symbol=GBPUSD|id=24|side=ASK|price=1.280000|quantity=1000|execId=117|filledPrice=1.280000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=118|filledPrice=1.280000|filledQuantity=1000|remainingQuantity=12000
type=FILLED||symbol=GBPUSD|id=19|side=ASK|price=1.350000|quantity=7000|execId=119|filledPrice=1.350000|filledQuantity=7000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=120|filledPrice=1.350000|filledQuantity=7000|remainingQuantity=5000
type=FILLED||symbol=GBPUSD|id=20|side=ASK|price=1.350000|quantity=5000|execId=121|filledPrice=1.350000|filledQuantity=5000
type=FILLED||symbol=GBPUSD|id=52|side=BID|price=1.350000|quantity=15000|execId=122|filledPrice=1.350000|filledQuantity=5000
type=NEW||symbol=EURUSD|id=53|side=ASK|price=1.170000|quantity=10000|execId=123
type=FILLED||symbol=EURUSD|id=18|side=BID|price=1.170000|quantity=6000|execId=124|filledPrice=1.170000|filledQuantity=5000
type=PARTIALLY_FILLED||symbol=EURUSD|id=53|side=ASK|price=1.170000|quantity=10000|execId=125|filledPrice=1.170000|filledQuantity=5000|remainingQuantity=5000
type=NEW||symbol=EURUSD|id=54|side=BID|price=1.330000|quantity=15000|execId=126
type=FILLED||symbol=EURUSD|id=53|side=ASK|price=1.170000|quantity=10000|execId=127|filledPrice=1.170000|filledQuantity=5000
type=PARTIALLY_FILLED||symbol=EURUSD|id=54|side=BID|price=1.330000|quantity=15000|execId=128|filledPrice=1.170000|filledQuantity=5000|remainingQuantity=10000
type=FILLED||symbol=EURUSD|id=1|side=ASK|price=1.330000|quantity=7000|execId=129|filledPrice=1.330000|filledQuantity=5000
type=PARTIALLY_FILLED||symbol=EURUSD|id=54|side=BID|price=1.330000|quantity=15000|execId=130|filledPrice=1.330000|filledQuantity=5000|remainingQuantity=5000
type=FILLED||symbol=EURUSD|id=2|side=ASK|price=1.330000|quantity=5000|execId=131|filledPrice=1.330000|filledQuantity=5000
type=FILLED||symbol=EURUSD|id=54|side=BID|price=1.330000|quantity=15000|execId=132|filledPrice=1.330000|filledQuantity=5000
type=NEW||symbol=GBPUSD|id=55|side=ASK|price=1.150000|quantity=14000|execId=133
type=FILLED||symbol=GBPUSD|id=34|side=BID|price=1.150000|quantity=3000|execId=134|filledPrice=1.150000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=55|side=ASK|price=1.150000|quantity=14000|execId=135|filledPrice=1.150000|filledQuantity=1000|remainingQuantity=13000
type=FILLED||symbol=GBPUSD|id=35|side=BID|price=1.150000|quantity=5000|execId=136|filledPrice=1.150000|filledQuantity=5000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=55|side=ASK|price=1.150000|quantity=14000|execId=137|filledPrice=1.150000|filledQuantity=5000|remainingQuantity=8000
type=FILLED||symbol=GBPUSD|id=36|side=BID|price=1.150000|quantity=6000|execId=138|filledPrice=1.150000|filledQuantity=6000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=55|side=ASK|price=1.150000|quantity=14000|execId=139|filledPrice=1.150000|filledQuantity=6000|remainingQuantity=2000
type=NEW||symbol=GBPUSD|id=56|side=BID|price=1.350000|quantity=5000|execId=140
type=FILLED||symbol=GBPUSD|id=55|side=ASK|price=1.150000|quantity=14000|execId=141|filledPrice=1.150000|filledQuantity=2000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=56|side=BID|price=1.350000|quantity=5000|execId=142|filledPrice=1.150000|filledQuantity=2000|remainingQuantity=3000
type=FILLED||symbol=GBPUSD|id=21|side=ASK|price=1.350000|quantity=1000|execId=143|filledPrice=1.350000|filledQuantity=1000
type=PARTIALLY_FILLED||symbol=GBPUSD|id=56|side=BID|price=1.350000|quantity=5000|execId=144|filledPrice=1.350000|filledQuantity=1000|remainingQuantity=2000

"""
