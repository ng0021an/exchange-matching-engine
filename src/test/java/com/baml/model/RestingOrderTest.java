package com.baml.model;

import com.baml.matchingengine.data.*;
import com.baml.matchingengine.model.RestingOrder;
import com.baml.matchingengine.publish.ExecutionListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by QuanNguyenHuu on 1/7/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class RestingOrderTest {

    @Mock ExecutionListener executionListener;

    @Captor ArgumentCaptor<Message> executionCaptor;

    private RestingOrder restingOrder;
    private RestingOrder request;

    private void setUpOrder(String side, double price, long quantity) {
        restingOrder = new RestingOrder(new NewOrderRequest(new NewOrderRequestJson("EURUSD", side, price, quantity)), executionListener);
    }

    private void setUpRequest(String side, double price, long quantity) {
        request = new RestingOrder(new NewOrderRequest(new NewOrderRequestJson("EURUSD", side, price, quantity)), executionListener);
    }

    @Test
    public void should_not_match_request_with_worse_price() {
        setUpOrder("BID", 1.25, 1000);
        setUpRequest("ASK", 1.26, 1000);
        assertThat(restingOrder.isMatchable(request), is(false));

        setUpOrder("ASK", 1.24, 1000);
        setUpRequest("BID", 1.23, 1000);
        assertThat(restingOrder.isMatchable(request), is(false));
    }

    @Test
    public void should_match_request_with_better_price() {
        setUpOrder("BID", 1.25, 1000);
        setUpRequest("ASK", 1.24, 1000);
        assertThat(restingOrder.isMatchable(request), is(true));

        setUpOrder("ASK", 1.24, 1000);
        setUpRequest("BID", 1.25, 1000);
        assertThat(restingOrder.isMatchable(request), is(true));
    }

    @Test
    public void should_fully_fill_lower_liquidity_request_and_create_correct_trades_for_bid() {
        setUpOrder("BID", 1.25, 2000);
        setUpRequest("ASK", 1.24, 1000);

        restingOrder.fulfillNewRequest(request);

        assertThat(restingOrder.getQuantity(), is(1000l));
        assertThat(request.getQuantity(), is(0l));

        verify(executionListener, times(2)).onNewExecution(executionCaptor.capture());
        List<Message> executions = executionCaptor.getAllValues();

        assertThat(executions.size(), is(2));

        verifyExecution(executions.get(0), MessageType.PARTIALLY_FILLED, "EURUSD", Side.BID, 1.25, 2000,
                restingOrder.getNewOrderRequest(), 1.25, 1000l, 1000l);
        verifyExecution(executions.get(1), MessageType.FILLED, "EURUSD", Side.ASK, 1.24, 1000,
                request.getNewOrderRequest(), 1.25, 1000l, null);
    }

    @Test
    public void should_fully_fill_lower_liquidity_request_and_create_correct_trades_for_ask() {
        setUpOrder("ASK", 1.24, 5000);
        setUpRequest("BID", 1.25, 3000);

        restingOrder.fulfillNewRequest(request);

        assertThat(restingOrder.getQuantity(), is(2000l));
        assertThat(request.getQuantity(), is(0l));

        verify(executionListener, times(2)).onNewExecution(executionCaptor.capture());
        List<Message> executions = executionCaptor.getAllValues();

        assertThat(executions.size(), is(2));

        verifyExecution(executions.get(0), MessageType.PARTIALLY_FILLED, "EURUSD", Side.ASK, 1.24, 5000,
                restingOrder.getNewOrderRequest(), 1.24, 3000l, 2000l);
        verifyExecution(executions.get(1), MessageType.FILLED, "EURUSD", Side.BID, 1.25, 3000,
                request.getNewOrderRequest(), 1.24, 3000l, null);
    }

    @Test
    public void should_partially_fill_higher_liquidity_request_and_create_correct_trades_for_bid() {
        setUpOrder("BID", 1.25, 2000);
        setUpRequest("ASK", 1.24, 4000);

        restingOrder.fulfillNewRequest(request);

        assertThat(restingOrder.getQuantity(), is(0l));
        assertThat(request.getQuantity(), is(2000l));

        verify(executionListener, times(2)).onNewExecution(executionCaptor.capture());
        List<Message> executions = executionCaptor.getAllValues();

        assertThat(executions.size(), is(2));

        verifyExecution(executions.get(0), MessageType.FILLED, "EURUSD", Side.BID, 1.25, 2000,
                restingOrder.getNewOrderRequest(), 1.25, 2000l, null);
        verifyExecution(executions.get(1), MessageType.PARTIALLY_FILLED, "EURUSD", Side.ASK, 1.24, 4000,
                request.getNewOrderRequest(), 1.25, 2000l, 2000l);
    }

    @Test
    public void should_partially_fill_higher_liquidity_request_and_create_correct_trades_for_ask() {
        setUpOrder("ASK", 1.24, 4000);
        setUpRequest("BID", 1.25, 7000);

        restingOrder.fulfillNewRequest(request);

        assertThat(restingOrder.getQuantity(), is(0l));
        assertThat(request.getQuantity(), is(3000l));

        verify(executionListener, times(2)).onNewExecution(executionCaptor.capture());
        List<Message> executions = executionCaptor.getAllValues();

        assertThat(executions.size(), is(2));

        verifyExecution(executions.get(0), MessageType.FILLED, "EURUSD", Side.ASK, 1.24, 4000,
                restingOrder.getNewOrderRequest(), 1.24, 4000l, null);
        verifyExecution(executions.get(1), MessageType.PARTIALLY_FILLED, "EURUSD", Side.BID, 1.25, 7000,
                request.getNewOrderRequest(), 1.24, 4000l, 3000l);
    }

    private void verifyExecution(Message execution, MessageType type, String symbol, Side side, double price, long quantity,
                                 NewOrderRequest newOrderRequest, Double filledPrice, Long filledQuantity, Long remainingQuantity) {
        assertThat(execution.getType(), is(type));
        assertThat(execution.getSymbol(), is(symbol));
        assertThat(execution.getSide(), is(side));
        assertThat(execution.getPrice(), is(price));
        assertThat(execution.getQuantity(), is(quantity));

        if (execution instanceof FilledOrder) {
            assertThat(((FilledOrder) execution).getNewOrderRequest(), is(newOrderRequest));
            assertThat(((FilledOrder) execution).getFilledPrice(), is(filledPrice));
            assertThat(((FilledOrder) execution).getFilledQuantity(), is(filledQuantity));
        }

        if (execution instanceof PartiallyFilledOrder) {
            assertThat(((PartiallyFilledOrder) execution).getNewOrderRequest(), is(newOrderRequest));
            assertThat(((PartiallyFilledOrder) execution).getFilledPrice(), is(filledPrice));
            assertThat(((PartiallyFilledOrder) execution).getFilledQuantity(), is(filledQuantity));
            assertThat(((PartiallyFilledOrder) execution).getRemainingQuantity(), is(remainingQuantity));
        }
    }
}
