package matchingengineintegration.steps;

import com.baml.matchingengine.ExchangeMatchingEngineApp;
import com.baml.matchingengine.config.AppConfig;
import com.baml.matchingengine.data.NewOrderRequestJson;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes={ExchangeMatchingEngineApp.class, AppConfig.class})
@SpringBootTest(classes = {ExchangeMatchingEngineApp.class, AppConfig.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MatchingEngineIntegrationSteps {

    @Autowired
    private TestRestTemplate restTemplate;

    private String snapshot;
    private String requestFile;
    private String executionFile;

    @Given("^I send order requests$")
    public void i_send_order_requests(DataTable dataTable) {
        dataTable.asMaps(String.class,String.class).stream()
                .map(m -> new HttpEntity<>(new NewOrderRequestJson(
                        m.get("symbol"),
                        m.get("side"),
                        Double.valueOf(m.get("price")),
                        Long.valueOf(m.get("quantity"))
                ))).forEach(requestJson -> restTemplate.postForObject("/api/new", requestJson, String.class));
    }

    @When("^I get snapshot for (.*)$")
    public void i_get_snapshot_for(String symbol) {
        snapshot = restTemplate.getForObject(String.format("/api/orderbook/%s", symbol), String.class).replaceAll("time=[^|]*|", "");
    }

    @Then("^I should get snapshot as (.*)$")
    public void i_should_get_snapshot_as(String expectedSnapshot) {
        assertThat(snapshot, is(expectedSnapshot));
    }

    @When("^I get request file$")
    public void i_get_request_file() throws IOException {
        requestFile = readFile("./request.txt").replaceAll("time=[^|]*|", "");
    }

    @Then("^I should get request file as$")
    public void i_should_get_request_file_as(String expectedRequestFile) {
        assertThat(requestFile, is(expectedRequestFile));
    }

    @When("^I get execution file$")
    public void i_get_execution_file() throws IOException {
        executionFile = readFile("./execution.txt").replaceAll("time=[^|]*|", "");
    }

    @Then("^I should get execution file as$")
    public void i_should_get_execution_file_as(String expectedExecutionFile) {
        assertThat(executionFile, is(expectedExecutionFile));
    }

    private String readFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }
}
