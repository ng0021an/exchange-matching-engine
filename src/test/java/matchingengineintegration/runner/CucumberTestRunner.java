package matchingengineintegration.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by QuanNguyenHuu on 30/6/18.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        format = {"pretty", "html:target/cucumber"},
        features = "src/test/resources",
        glue = "matchingengineintegration.steps")
public class CucumberTestRunner {
}
